FROM node:latest

WORKDIR /api
COPY api/ ./
RUN npm install

EXPOSE 8081/tcp

ENTRYPOINT ["npm", "run", "serve:dev"]