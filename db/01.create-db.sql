/**
 * Database: Tendervendor
 * Author: Talin Paul <talinpaul@outlook.com>
 * Version: 0.0.1
 */

-- Create database tendervendor
CREATE DATABASE sample_node;

-- Create user 'tvroot' & password
CREATE USER 'node'@'%' IDENTIFIED BY 'toor';
-- Grant privileges on database 'tendervendor' to user 'tvroot'
GRANT SUPER ON *.* TO 'node'@'%';
GRANT ALL PRIVILEGES ON sample_node.* TO 'node'@'%';

FLUSH PRIVILEGES;
